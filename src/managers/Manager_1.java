package managers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Manager_1 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir une requête :");
		String str = sc.nextLine();
		System.out.println("Votre requête : " + str);
		
		String reponse;
		Socket leSocket;
		PrintStream fluxSortieSocket;
		BufferedReader fluxEntreeSocket;
		
		try {
			// creation d'une socket et connexion la machine marine sur le
			// port numéro 28141
			leSocket = new Socket("127.0.0.1", 28141);
			System.out.println("TCP - Connecté sur : " + leSocket);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(leSocket.getInputStream()));
			PrintStream ps = new PrintStream(leSocket.getOutputStream());
			
			ps.println(str);
			String rep = br.readLine();
			
			System.out.println("Reponse du serveur : " + rep);			

			leSocket.close();
		} // try
		catch (UnknownHostException ex) {
			System.err.println("Machine inconnue : " + ex);
			ex.printStackTrace();
		} catch (IOException ex) {
			System.err.println("Erreur : " + ex);
			ex.printStackTrace();
		}
	}		
}
