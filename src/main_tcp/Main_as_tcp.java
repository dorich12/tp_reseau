package main_tcp;

import java.io.*;
import java.net.*;

public class Main_as_tcp extends Object {
	
	public static void main (String args[]) { 
	
		ServerSocket socketEcoute;
		Socket socketService;
		InputStream entreeSocket;
		OutputStream sortieSocket;

		try {
			// création du socket d'écoute (port numéro 28141)
			socketEcoute = new ServerSocket(28141);
			while (true) {
				// attente d�une demande de connexion pour accepter
				socketService = socketEcoute.accept();
				System.out.println("Nouvelle connexion : " + socketService);
				
				BufferedReader br = new BufferedReader(new InputStreamReader(socketService.getInputStream()));
				PrintStream ps = new PrintStream(socketService.getOutputStream());
				
				String req = br.readLine();
				String rep = "Connexion bien déroulée";
				ps.println(rep+" pour la requête : "+req);
				
				// 	AUTRE FACON :				
				//				// récupération des flux d'entrée/sortie de la socket de service
				//				entreeSocket = socketService.getInputStream();
				//				sortieSocket = socketService.getOutputStream();
				//				try {
				//					String req = entreeSocket.readLine();
				//					int b = 0;
				//					while (b != -1) {
				//						b = entreeSocket.read();
				//						sortieSocket.write(b);
				//					} // while
				//					System.out.println("Fin de connexion");
				//				} // try
				//				catch (IOException ex)
				//				{
				//					// fin de connexion
				//					System.out.println("Fin de connexion : "+ex);
				//					ex.printStackTrace();
				//				}
				// FIN AUTRE FACON				
				
				socketService.close();
			} // while (true)
		} // try
		catch (Exception ex)
		{
			// erreur de connexion
			System.err.println("Une erreur est survenue : "+ex);
			ex.printStackTrace();
		}
	}
} // class