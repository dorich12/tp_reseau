package main_tcp;

import java.io.*; 
import java.net.*;

public class Main_client_tcp extends Object { 
	
	public static void main (String args[]) {
	
		String reponse;
		Socket leSocket;
		PrintStream fluxSortieSocket;
		BufferedReader fluxEntreeSocket;
		
		try {
			// creation d'une socket et connexion la machine marine sur le
			// port numéro 7
			leSocket = new Socket("127.0.0.1", 28141);
			System.out.println("TCP - Connecté sur : " + leSocket);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(leSocket.getInputStream()));
			PrintStream ps = new PrintStream(leSocket.getOutputStream());
			
			String req = "CHK Toto toto";
			ps.println(req);
			String rep = br.readLine();

			System.out.println("Reponse du serveur : " + rep);

			// AUTRE FACON			
			//			// création d'un flux de type PrintStream lié au flux de sortie de
			//			// la socket
			//			fluxSortieSocket = new PrintStream(leSocket.getOutputStream());
			//			// creation d'un flux de type BufferedReader lié au flux d'entrée de
			//			// la socket
			//			fluxEntreeSocket = new BufferedReader(new InputStreamReader(
			//					leSocket.getInputStream()));
			//			// envoi de données vers le serveur
			//			fluxSortieSocket.println("Bonjour le monde!");
			//			// attente puis réception de données envoyées par le serveur
			//			reponse = fluxEntreeSocket.readLine();
			//			System.out.println("Reponse du serveur : " + reponse);
			// FIN AUTRE FACON			

			leSocket.close();
		} // try
		catch (UnknownHostException ex) {
			System.err.println("Machine inconnue : " + ex);
			ex.printStackTrace();
		} catch (IOException ex) {
			System.err.println("Erreur : " + ex);
			ex.printStackTrace();
		}

	}
} // class
