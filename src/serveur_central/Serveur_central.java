package serveur_central;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import model.GestionProtocole;
import thread.Multi_Thread_Serveur;

public class Serveur_central extends Thread {
	final static int port = 28141; 
	final static int taille = 1024; 
	
	GestionProtocole gp;
	
	private ServerSocket socketEcoute;
	private Socket socketService;
	
	public Serveur_central(GestionProtocole gp){
		this.gp = gp;
	}
	 
	public void run(){ // SERVEUR TCP
		try {
			// création du socket d'écoute (port numéro 28141)
			socketEcoute = new ServerSocket(28141);
			while (true) {
				socketService = socketEcoute.accept();
				Multi_Thread_Serveur mts = new Multi_Thread_Serveur(socketService);
				mts.run();
			}
		} // try
		catch (Exception ex)
		{
			// erreur de connexion
			System.err.println("Une erreur est survenue : "+ex);
			ex.printStackTrace();
		}
	}
	
	public static void main(String[] args) {

		Serveur_central st = new Serveur_central(new GestionProtocole());
		st.start();

		byte[] tampon = new byte[taille];
		GestionProtocole gp = new GestionProtocole();

		try {
			DatagramSocket ds = new DatagramSocket(port);
			
			 while(true) 
		      { 
				DatagramPacket dp = new DatagramPacket(tampon, tampon.length);
				try {
					ds.receive(dp);
					
					String req = new String(tampon, 0, dp.getLength()); // extraction de la requete
					String rep = gp.traiterReq(req); // appel gestionprotocol pour traiter la requete
					dp.setData(rep.getBytes());
					
					System.out.println("Adresse destinataire : "+dp.getAddress()); 
					
					ds.send(dp);
					
				} catch (IOException e) {
					e.printStackTrace();
					ds.close();
				}
		      }
			 
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
	}
}
