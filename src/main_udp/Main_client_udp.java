package main_udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Main_client_udp {
	
	final static int taille = 1024; 

	public static void main(String[] args) throws IOException {
		
		byte tampon[] = new byte[taille];
		
		try {
			
			InetAddress serveur = InetAddress.getByName("127.0.0.1"); 
			String req = "CHK Toto toto";
		    DatagramPacket dataSent = new DatagramPacket(req.getBytes(),req.getBytes().length,serveur,28141); 
		    DatagramSocket socket = new DatagramSocket(); 
		  
		    socket.send(dataSent); 

		    DatagramPacket dataRecieved = new DatagramPacket(new byte[tampon.length],tampon.length); 
		    socket.receive(dataRecieved); 
		    System.out.println("UDP - Données reçues : " + new String(dataRecieved.getData())); 
		    System.out.println("De : " + dataRecieved.getAddress() + ":" + dataRecieved.getPort()); 
			
		    socket.close();

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
