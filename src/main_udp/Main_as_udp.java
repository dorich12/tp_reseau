package main_udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import model.GestionProtocole;

public class Main_as_udp {
	
	final static int port = 28141; 
	final static int taille = 1024; 

	public static void main(String[] args) {
		
		byte[] tampon = new byte[taille];
		GestionProtocole gp = new GestionProtocole();

		try {
			DatagramSocket ds = new DatagramSocket(port);
			
			 while(true) 
		      { 
				DatagramPacket dp = new DatagramPacket(tampon, tampon.length);
				try {
					ds.receive(dp);
					
					String req = new String(tampon, 0, dp.getLength()); // extraction de la requete
					String rep = gp.traiterReq(req); // appel gestionprotocol pour traiter la requete
					dp.setData(rep.getBytes());
					
					System.out.println("Adresse destinataire : "+dp.getAddress()); 
					
					ds.send(dp);
					
				} catch (IOException e) {
					e.printStackTrace();
					ds.close();
				}
		      }
			 
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
	}

}
