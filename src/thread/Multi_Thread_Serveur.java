package thread;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import model.GestionProtocole;

public class Multi_Thread_Serveur implements Runnable {
	
	private ServerSocket socketEcoute;
	private Socket socketService;
	
	GestionProtocole gp;

	public Multi_Thread_Serveur(Socket SocketService) {
		this.gp = new GestionProtocole();
		
		this.socketService = SocketService;
	}
	
	@Override
	public void run() {
		try {
			// attente d'une demande de connexion pour accepter
			System.out.println("Nouvelle connexion multi thread : " + socketService);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(socketService.getInputStream()));
			PrintStream ps = new PrintStream(socketService.getOutputStream());
			
			String req = br.readLine();
			System.out.println("Req  test : " + req);
			String rep = gp.traiterReq(req); // appel de gestionprotocol pour traiter la requete
			ps.println("Réponse : "+ rep +" pour la requête : "+req);
			
			socketService.close();
			
		} catch(Exception ex) {
			// erreur de connexion
			System.err.println("Une erreur est survenue : "+ex);
			ex.printStackTrace();
		}
	}
}
